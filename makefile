
# Project Properties
PROJECT := redmix-check
SRCS := $(wildcard ./src/*.cpp)
OBJS := $(patsubst ./src/%.cpp, ./build/%.o, $(SRCS))
BUILDDIR := ./build
BINDIR := ./bin
INCDIR := ./include

# Platform properties
## UNIX
GCC := g++
CPPFLAGS :=
CXXFLAGS :=
LDFLAGS :=

## Windows


# Unix Builds

all: builddir bindir $(BINDIR)/$(PROJECT)

$(BINDIR)/$(PROJECT): $(OBJS)
	$(GCC) $(LDFLAGS) -o "$@" $^

$(BUILDDIR)/%.o: ./src/%.cpp
	$(GCC) $(CXXFLAGS) $(CPPFLAGS) -c -o "$@" "$<"

bindir:
	if [ ! -d $(BINDIR) ] ; then mkdir $(BINDIR) ; fi

builddir:
	if [ ! -d $(BUILDDIR) ] ; then mkdir $(BUILDDIR) ; fi
